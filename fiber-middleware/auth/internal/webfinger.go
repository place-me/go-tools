package idp

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
)

type webfingerDocument struct {
	Issuer                      string   `json:"issuer"`
	AuthorizationEndpoint       string   `json:"authorization_endpoint"`
	JwksURI                     string   `json:"jwks_uri"`
	ResponseTypesSupported      []string `json:"response_types_supported"`
	SubjectTypesSupported       []string `json:"subject_types_supported"`
	IDTokenSigningAlgsSupported []string `json:"id_token_signing_alg_values_supported"`
	ClaimsSupported             []string `json:"claims_supported"`
}

type keys struct {
	Use string `json:"use"`
	Kty string `json:"kty"`
	Kid string `json:"kid"`
	Alg string `json:"alg"`
	N   string `json:"n"`
	E   string `json:"e"`
}

type keyset struct {
	Keys []keys `json:"keys"`
}

func (prv *OidcProvider) discoveryEP(ctx *fiber.Ctx) error {
	ctx.Set("Access-Control-Allow-Origin", "*")
	ctx.Set("Content-Type", "application/json")

	resp := webfingerDocument{
		Issuer:                      prv.cfg.Issuer,
		AuthorizationEndpoint:       prv.cfg.Issuer + "/authorize",
		JwksURI:                     prv.cfg.Issuer + "/keys",
		ResponseTypesSupported:      []string{"code", "token"},
		SubjectTypesSupported:       []string{"public"},
		IDTokenSigningAlgsSupported: []string{"RS256"},
		ClaimsSupported:             []string{"iss", "sub", "aud", "exp", "iat", "email", "email_verified"},
	}

	err := ctx.JSON(resp)
	if err != nil {
		return fmt.Errorf("failed to encode response: %w", err)
	}
	return nil
}

func (prv *OidcProvider) keysEP(ctx *fiber.Ctx) error {
	resp := keyset{
		Keys: []keys{
			{
				Use: "sig",
				Kty: "RSA",
				Kid: "demo",
				Alg: "RS256",
				N:   prv.n,
				E:   prv.e,
			},
		},
	}

	ctx.Set("Content-Type", "application/json")
	ctx.Set("Access-Control-Allow-Origin", "*")

	err := ctx.JSON(resp)
	if err != nil {
		return fmt.Errorf("failed to encode response: %w", err)
	}
	return nil
}
