package context

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	log "github.com/sirupsen/logrus"
)

func New(config ...Config) fiber.Handler {

	// Set default config
	cfg := configDefault(config...)

	return func(ctx *fiber.Ctx) error {

		// Don't execute middleware if Filter returns true
		if cfg.Filter != nil && cfg.Filter(ctx) {
			return ctx.Next()
		}

		var logger *log.Entry

		if ctx.Locals("logger") == nil {
			logger = log.NewEntry(log.New())
		} else {
			logger = ctx.Locals("logger").(*log.Entry)
		}

		required := false
		if cfg.Required != nil {
			required = cfg.Required(ctx)
		}

		var headerVal string
		ctx.Request().Header.VisitAll(func(key, value []byte) {
			name := utils.UnsafeString(key)
			if strings.EqualFold(name, cfg.HeaderName) {
				headerVal = utils.CopyString(utils.UnsafeString(value))
			}
		})

		if len(headerVal) <= 0 {
			if required {
				logger.Warn("No placeme context found for required request.")
				ctx.Response().AppendBodyString("Missing 'Placeme-Context' header for request")
				return ctx.SendStatus(400)
			} else {
				logger.Info("No placeme context found request.")
				return ctx.Next()
			}
		}

		contextParts := strings.Split(headerVal, ",")
		amountOfParts := len(contextParts)
		if amountOfParts == 1 {
			ctx.Locals("userprofileId", contextParts[0])
		} else if amountOfParts == 2 {
			ctx.Locals("userprofileId", contextParts[0])
			ctx.Locals("tenantId", contextParts[1])
		} else {
			logger.Warnf("Malformed placeme-context header for request: %s", headerVal)
			return ctx.Next()
		}

		return ctx.Next()
	}
}
