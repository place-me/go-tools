package context

import (
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

const TENANT = "65eeaf07-a308-4663-9a1b-abbed7867a62"
const USER = "30cecb4c-b845-4d79-9503-6b2b8e8746a8"

func Test_Context_Default_HeaderName(t *testing.T) {
	app := fiber.New()

	app.Use(New())

	app.Get("/", func(c *fiber.Ctx) error {
		utils.AssertEqual(t, TENANT, c.Locals("tenantId"), "Tenant Id")
		utils.AssertEqual(t, USER, c.Locals("userprofileId"), "User Id")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("Placeme-Context", fmt.Sprintf("%s,%s", USER, TENANT))

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}

func Test_Context_Custom_HeaderName(t *testing.T) {
	app := fiber.New()

	app.Use(New(Config{
		HeaderName: "foobar",
	}))

	app.Get("/", func(c *fiber.Ctx) error {
		utils.AssertEqual(t, TENANT, c.Locals("tenantId"), "Tenant Id")
		utils.AssertEqual(t, USER, c.Locals("userprofileId"), "User Id")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("foobar", fmt.Sprintf("%s,%s", USER, TENANT))

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}

func Test_Context_User_Only(t *testing.T) {
	app := fiber.New()

	app.Use(New())

	app.Get("/", func(c *fiber.Ctx) error {
		utils.AssertEqual(t, nil, c.Locals("tenantId"), "Tenant Id")
		utils.AssertEqual(t, USER, c.Locals("userprofileId"), "User Id")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("Placeme-Context", USER)

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}

func Test_Context_Missing_Header(t *testing.T) {
	app := fiber.New()

	app.Use(New())

	app.Get("/", func(c *fiber.Ctx) error {
		utils.AssertEqual(t, nil, c.Locals("tenantId"), "Tenant Id")
		utils.AssertEqual(t, nil, c.Locals("userprofileId"), "User Id")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}

func Test_Context_Missing_Header_Required_Request(t *testing.T) {
	app := fiber.New()

	app.Use(New(Config{
		Required: func(c *fiber.Ctx) bool { return true },
	}))

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 400, resp.StatusCode, "Status code")
}

func Test_Context_Skip(t *testing.T) {
	app := fiber.New()

	app.Use(New(Config{
		Filter: func(c *fiber.Ctx) bool { return true },
	}))

	app.Get("/", func(c *fiber.Ctx) error {
		utils.AssertEqual(t, nil, c.Locals("tenantId"), "Tenant Id")
		utils.AssertEqual(t, nil, c.Locals("userprofileId"), "User Id")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("foobar", fmt.Sprintf("%s,%s", USER, TENANT))

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}

func Test_Context_Malformed_Header(t *testing.T) {
	app := fiber.New()

	app.Use(New())

	app.Get("/", func(c *fiber.Ctx) error {
		utils.AssertEqual(t, nil, c.Locals("tenantId"), "Tenant Id")
		utils.AssertEqual(t, nil, c.Locals("userprofileId"), "User Id")
		return c.SendStatus(200)
	})

	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("Placeme-Context", fmt.Sprintf("%s,%s,foobar", USER, TENANT))

	resp, err := app.Test(req)
	utils.AssertEqual(t, nil, err, "app.Test(req)")
	utils.AssertEqual(t, 200, resp.StatusCode, "Status code")
}
